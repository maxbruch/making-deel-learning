#! /usr/bin/env python
# -*- coding: utf-8
import numpy as np
import matplotlib.pyplot as plt

X=np.arange(0, 6, 0.1)
Y1=np.sin(X)
Y2=np.cos(X)

plt.plot(X,Y1, label="sin")
plt.plot(X,Y2, linestyle="--", label="cos")
plt.xlabel("x")
plt.ylabel("y")
plt.title("sin & cos")
plt.legend()
plt.show()


