#! /usr/bin/env python
# -*- coding: utf-8

import numpy as np
import andnp as a
import ornp as o
import nandnp as n

def XOR(x1,x2):
    s1 = n.NAND(x1,x2)
    s2 = o.OR(x1,x2)
    y = a.AND(s1,s2)
    return y

print(XOR(0,0))
print(XOR(0,1))
print(XOR(1,0))
print(XOR(1,1))
